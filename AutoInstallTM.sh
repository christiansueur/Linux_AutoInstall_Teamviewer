#!/bin/bash

############################################################
# Auto install of teamviewer
# ######################################################## #
# :Author: Christian SUEUR
# :Contact: contact@christiansueur.com
############################################################

# Test if teamviewer is already installed
dpkg -s teamviewer &> /dev/null
if [ $? -eq 0 ];then
	echo -e "\e[31;1m Teamviewer is already installed \e[0m"
	exit 1
else
	#Run the download and install of teamviewer
	#make an update && upgrade
	echo -e "\e[41;1m WARNING : Auto update And upgrade\e[0m"
	sleep 5
	sudo apt-get update -y && sudo apt-get upgrade

	# Test if wget is installed
	if ! wget --version &>/dev/null; then
		echo -e "\e[31;1m Warning : Wget is necessary \e[0m"
		sleep 3
		#Install wget
		sudo apt-get install wget -y
	fi

	# Go to /tmp
	cd /tmp || exit
	# Install dependency
	echo -e "\e[32;1m Install dependencies \e[0m"
	sleep 5
	sudo apt-get install -y fontconfig fontconfig-config fonts-dejavu-core iso-codes libavahi-client3 libavahi-common-data libavahi-common3 libcap2-bin libcups2 libdouble-conversion1 libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libdrm2 libegl1-mesa libevdev2 libfontconfig1 libgbm1 libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgraphite2-3 libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libgudev-1.0-0 libharfbuzz0b libice6 libicu57 libinput-bin libinput10 libjpeg62-turbo libllvm3.9 libmtdev1 liborc-0.4-0 libpciaccess0 libpcre16-3 libproxy1v5 libqt5core5a libqt5dbus5 libqt5gui5 libqt5network5 libqt5opengl5 libqt5printsupport5 libqt5qml5 libqt5quick5 libqt5sql5 libqt5webkit5 libqt5widgets5 libqt5x11extras5 libsensors4 libsm6 libwacom-common libwacom2 libwayland-client0 libwayland-server0 libwebp6 libx11-6 libx11-data libx11-xcb1 libxau6 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-present0 libxcb-randr0 libxcb-render-util0 libxcb-render0 libxcb-shape0 libxcb-shm0 libxcb-sync1 libxcb-util0 libxcb-xfixes0 libxcb-xinerama0 libxcb-xkb1 libxcb1 libxcomposite1 libxdamage1 libxdmcp6 libxext6 libxfixes3 libxi6 libxkbcommon-x11-0 libxkbcommon0 libxml2 libxrender1 libxshmfence1 libxslt1.1 libxxf86vm1 qml-module-qtgraphicaleffects qml-module-qtquick-controls qml-module-qtquick-dialogs qml-module-qtquick-layouts qml-module-qtquick-privatewidgets qml-module-qtquick-window2 qml-module-qtquick2 x11-common

	# Download the lastest version of teamviewer
    
    MACHINE_TYPE=$(uname -m)
    if [ "${MACHINE_TYPE}" == 'x86_64' ]; then
        TmFile="/tmp/teamviewer_amd64.deb"
        OsVersion=64
    else
        TmFile="/tmp/teamviewer_i386.deb"
        OsVersion=32
    fi

    if [ ! -f $TmFile ]; then
        echo -e "\e[32;1m Download teamviewer \e[0m"
    	sleep 5
        if [ $OsVersion -eq 64 ] ; then
    	    wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
        else
            wget https://download.teamviewer.com/download/linux/teamviewer_i386.deb
        fi
    fi
    
    # Install package
    echo -e "\e[32;1m Install : teamviewer \e[0m"
    if [ $OsVersion -eq 64 ] ; then
        sudo dpkg -i teamviewer_amd64.deb
    else
        sudo dpkg -i teamviewer_i386.deb
    fi
    	echo -e "\e[33;1m Install : OK  \e[0m"
fi

